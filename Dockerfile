FROM alpine
EXPOSE 8080
CMD while true; do { echo -e "HTTP/1.1 200 OK\r\n"; [ $(( $RANDOM % 2 )) -eq 1 ] && echo head || echo tail; } | nc -l -p 8080; done
